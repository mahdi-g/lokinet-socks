# Caddy routed with Lokinet inside Docker

Run caddy server pre-configured with Lokinet. Running a Lokinet SNApp has never been easier.

## Compiling

**Attention**: you don't need to do this! There's already a pre-built image on [DockerHub](https://hub.docker.com/r/massiveboxe/caddy-lokinet). You might want to do compile the image by yourself only if the DockerHub one doesn't work on your architecture or you don't trust that image.

After cloning the repository and CD-ing in the correct folder, run:   
`sudo docker build . -t massiveboxe/caddy-lokinet`

## Running

**Read all section before running!**

`sudo docker run --name caddy-lokinet --cap-add=NET_ADMIN --device=/dev/net/tun -v DATA_PATH:/caddy/data --restart unless-stopped massiveboxe/caddy-lokinet:latest ` 

Replace DATA_PATH with the absolute path to the folder that contains the Caddyfile, static files you want to serve, and where your Private Key will be stored. Do not replace NET_ADMIN.  
We have included a data folder that works with the repo. If you want to use that one, replace DATA_PATH with `/ABSOLUTE_PATH_OF_FOLDER_WHERE_YOU_CLONED_THIS_REPO/caddy-lokinet/data`. Note that the path must be absolute, so it must start with `/` (root).

You should see the Lokinet bootstrap warns and infos (they should vaguely look like [this](https://telegra.ph/file/2deda318c96a7bac257ae.jpg), it's normal to have warnings), after the bootstrap you will get a message saying `Here is your address: SOMETHINGSOMETHING.loki`, and it will be saved inside an `address.txt` file inside your data folder.

## Troubleshooting

**I see no Lokinet startup logs**  

1. `sudo docker exec -it caddy-lokinet /bin/sh `
2. `/lokinet/build/daemon/lokinet`
   - If you get a messages that includes "`what():  Address already in use`", remove the `address.txt` from the data folder and try again.  
   - If your error message contains "`Illegal instruction (core dumped)`", you'll have to build the standalone Lokinet image from source ([here's how](https://codeberg.org/massivebox/dockerlokinet/src/branch/main/lokinet/README.md)) and then this image again ([guide](#Compiling)). But don't worry, it's just as easy as building this container, and it still does not require to install Lokinet in the host machine.

**I see an error message/warning in the Lokinet bootstrap, should I worry?**

- Generally, all warns (yellow) can be ignored. 
- If you get an error saying `Cannot open /dev/net/tun: No such file or directory`, make sure you have included `--cap-add=NET_ADMIN --device=/dev/net/tun` when running `docker run`. This error could also happen when trying to use this container in Windows, where I don't think there's a workaround other than getting a decent OS.

**The address changes every time I start the container**

Make sure you have included `-v DATA_PATH:/caddy/data` in the `docker run` command, and replaced `DATA_PATH` with the actual absolute path where the container data is located. More info above, in the "Running" section.

## Support, Licensing, etc.

For support, licensing information, etcetera take a look at the [main README.](https://codeberg.org/massivebox/lokinet-docker/src/branch/main/README.md)