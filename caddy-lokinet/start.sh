#!/bin/bash

echo 'nameserver 127.3.2.1' > /etc/resolv.conf
nohup /lokinet/build/daemon/lokinet &

address=""
if [ -e data/address.txt ]; then
	address=`cat data/address.txt`
	echo "SUCCESS: Here is your address: $address"
else
	for i in {0..100}; do
		echo "Attempt $i/100 at getting my address."
		sleep 2s
		address=`curl -s http://probably.loki/echo.sh`
		if [ -n "$address" ]; then
			echo "$address" > data/address.txt
			echo "SUCCESS: Here is your NEW address: $address"
			break
		fi
	done
fi

if [ -z "$address" ]; then
	echo "ERROR: Timeout for retreiving address. Aborting."
	exit 1
fi

./caddy run --config ./data/Caddyfile
